#include "action_layer.h"
#include "sendstring_french.h"

/************************************************************************************/
/* Unicode */
/************************************************************************************/

enum unicode_names {
  A_GR_U, // À
  C_CE_U, // Ç
  E_GR_U, // È
  E_AI_U  // É
};

const uint32_t PROGMEM unicode_map[] = {
    [A_GR_U] = 0xC0, [C_CE_U] = 0xC7, [E_GR_U] = 0xC8, [E_AI_U] = 0xC9};

/************************************************************************************/
/* Macros */
/************************************************************************************/

enum custom_keycodes { TG_WORKMAN = SAFE_RANGE };

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  if (record->event.pressed) {
    switch (keycode) {
    case TG_WORKMAN:
      set_single_persistent_default_layer(
          (1 + get_highest_layer(default_layer_state)) % 2);
      break;
    }
  }
  return true;
};

/************************************************************************************/
/* Leader key */
/************************************************************************************/

void leader_end_user(void) {
  static bool writing_email = false;

  if (leader_sequence_one_key(QK_LEAD)) {
    if (writing_email) {
      SEND_STRING(EMAIL_SUFFIX);
    }
    writing_email = false;
  }
  if (leader_sequence_one_key(KC_E)) {
    SEND_STRING(EMAIL_PREFIX);
    writing_email = true;
  }
  if (leader_sequence_one_key(KC_R)) {
    SEND_STRING(WORK_EMAIL);
  }
  if (leader_sequence_one_key(KC_T)) {
    SEND_STRING(PHONE);
  }
}

/************************************************************************************/
/* Tap dances */
/************************************************************************************/

typedef struct {
  bool is_press_action;
  int state;
} tap;

enum {
  SINGLE_TAP = 1,
  SINGLE_HOLD = 2,
  DOUBLE_TAP = 3,
  DOUBLE_HOLD = 4,
  DOUBLE_SINGLE_TAP = 5, // send two single taps
  TRIPLE_TAP = 6,
  TRIPLE_HOLD = 7
};

// Tap dance enum
enum { X_CTL = 0, X_OS = 1 };

int cur_dance(tap_dance_state_t *state) {
  if (state->count == 1) {
    if (!state->pressed)
      return SINGLE_TAP;
    else
      return SINGLE_HOLD;
  } else if (state->count == 2) {
    if (!state->pressed && state->interrupted)
      return DOUBLE_SINGLE_TAP;
    else if (!state->pressed)
      return DOUBLE_TAP;
    else
      return DOUBLE_HOLD;
  }
  if (state->count == 3) {
    if (!state->pressed)
      return TRIPLE_TAP;
    else
      return TRIPLE_HOLD;
  } else
    return 8;
}

bool is_a_tap(tap_dance_state_t *state) { return !state->pressed; }

// X_CTL
static tap x_ctl_tap_state = {.is_press_action = true, .state = 0};

void x_ctl_tap(tap_dance_state_t *state, void *user_data) {
  x_ctl_tap_state.state = cur_dance(state);
  switch (x_ctl_tap_state.state) {
  case DOUBLE_TAP:
  case DOUBLE_HOLD:
    register_code(KC_LSFT);
  case SINGLE_TAP:
  case SINGLE_HOLD:
    register_code(KC_LCTL);
    break;
  }
}

void x_ctl_reset(tap_dance_state_t *state, void *user_data) {
  unregister_code(KC_LSFT);
  unregister_code(KC_LCTL);
  x_ctl_tap_state.state = 0;
}

// X_OS
static tap x_os_tap_state = {.is_press_action = true, .state = 0};

void x_os_tap(tap_dance_state_t *state, void *user_data) {
  x_os_tap_state.state = cur_dance(state);
  switch (x_os_tap_state.state) {
  case DOUBLE_HOLD:
    layer_on(7);
  case SINGLE_HOLD:
    register_code(KC_LGUI);
    break;
  }
}

void x_os_finished(tap_dance_state_t *state, void *user_data) {
  x_os_tap_state.state = cur_dance(state);
  if (is_a_tap(state)) {
    layer_off(7);
    unregister_code(KC_LGUI);
  }
  switch (x_os_tap_state.state) {
  case DOUBLE_SINGLE_TAP:
  case DOUBLE_TAP:
    tap_code(KC_SPC);
  case SINGLE_TAP:
    register_code(KC_SPC);
    break;
  }
}

void x_os_reset(tap_dance_state_t *state, void *user_data) {
  layer_off(7);
  unregister_code(KC_LGUI);
  unregister_code(KC_SPC);
  x_os_tap_state.state = 0;
}

// Tap dance actions
tap_dance_action_t tap_dance_actions[] = {
    [X_CTL] = ACTION_TAP_DANCE_FN_ADVANCED(x_ctl_tap, NULL, x_ctl_reset),
    [X_OS] = ACTION_TAP_DANCE_FN_ADVANCED(x_os_tap, x_os_finished, x_os_reset)};

uint16_t get_tapping_term(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {
  case QK_TAP_DANCE ... QK_TAP_DANCE_MAX:
    return 700;
  default:
    return TAPPING_TERM;
  }
}

/************************************************************************************/
/* Music mode */
/************************************************************************************/

bool music_mask_user(uint16_t keycode) {
  switch (keycode) {
  case MO(2):
  case MO(3):
  case MO(7):
    return false;
  default:
    return true;
  }
}

/************************************************************************************/
/* Layout */
/************************************************************************************/
