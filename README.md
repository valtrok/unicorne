# Unicorne keymap and configuration

This repository contains my unicorne keyboard QMK configuration. Feel free to use it and modify it. It is made for a unicorne (TODO: add link) keyboard.

## Setup

Clone and make a link the keymap to your qmk installation folder

```
git clone https://gitlab.com/valtrok/unicorne.git
cd unicorne
./scripts/link_keymap.sh
```

Setup private_defs.c file with all private variables

```
./scripts/set_private_defs.sh
```

Optional: Convert planck keymap

```
python scripts/planck2unicorne.py /path/to/planck/keymap.json keymap.json
```

Generate keymap.c

```
./scripts/make_keymap.sh
```

Flash your keyboard

```
./scripts/flash.sh
```
