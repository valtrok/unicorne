#pragma once

#define LEADER_PER_KEY_TIMING
#define LEADER_TIMEOUT 250
#define TAPPING_TERM 200
#define TAPPING_TERM_PER_KEY
#define UNICODE_SELECTED_MODES UNICODE_MODE_LINUX
#define PERMISSIVE_HOLD
#define DEFAULT_LAYER_SONGS                                                    \
  { SONG(QWERTY_SOUND), SONG(WORKMAN_SOUND) }
