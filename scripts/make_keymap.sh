#!/bin/bash

QMK_HOME=$(qmk config -ro user.qmk_home | cut -d= -f2)
KEYMAP_PATH=$QMK_HOME/keyboards/unicorne/keymaps/mine

qmk json2c $KEYMAP_PATH/base_keymap.json -o $KEYMAP_PATH/base_keymap.c

head -n 1 $KEYMAP_PATH/base_keymap.c > $KEYMAP_PATH/keymap.c
cat $KEYMAP_PATH/private_defs.c >> $KEYMAP_PATH/keymap.c
cat $KEYMAP_PATH/custom.c >> $KEYMAP_PATH/keymap.c
tail -n +2 $KEYMAP_PATH/base_keymap.c >> $KEYMAP_PATH/keymap.c
rm $KEYMAP_PATH/base_keymap.c
