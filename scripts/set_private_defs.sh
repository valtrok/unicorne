#!/bin/bash

QMK_HOME=$(qmk config -ro user.qmk_home | cut -d= -f2)
KEYMAP_PATH=$QMK_HOME/keyboards/unicorne/keymaps/mine

read -p "EMAIL_PREFIX: " email_prefix
read -p "EMAIL_SUFFIX: " email_suffix
read -p "WORK_EMAIL: " work_email
read -p "PHONE: " phone

echo "#define EMAIL_PREFIX \"$email_prefix\"" > private_defs.c
echo "#define EMAIL_SUFFIX \"$email_suffix\"" >> private_defs.c
echo "#define WORK_EMAIL \"$work_email\"" >> private_defs.c
echo "#define PHONE \"$phone\"" >> private_defs.c
