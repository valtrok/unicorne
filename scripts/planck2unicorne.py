import argparse
import json
import pathlib

parser = argparse.ArgumentParser(
                    prog='planck2unicorne',
                    description='Convert Planck keymaps to Unicorne.')
parser.add_argument('input_file', type=pathlib.Path)
parser.add_argument('output_file', type=pathlib.Path)

args = parser.parse_args()

with open(args.input_file) as f:
    keymap = json.load(f)

for layer in keymap["layers"]:
    del layer[45:47]
    del layer[37:39]

keymap.update({
    "notes": "Unicorne configuration",
    "keyboard": "unicorne",
    "keymap": "unicorne_mine",
    "layout": "LAYOUT",
})

with open(args.output_file, "w") as f:
    json.dump(keymap, f)

print("Done")
